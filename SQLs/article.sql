select
   id
 , title
 , body
 , publish_date::date
 , updated_at::date updated_date
 from
   article
 where
       is_deleted = false
   and publish_date <= now()
 order by publish_date desc
 limit 5

