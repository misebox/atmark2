select
  id
, title
, body
, date_part('year', publish_date) publish_year
, updated_at::date updated_date
  from article
  where id = '<@aid>'
    and is_deleted = false
    and publish_date <= now()::date
;
