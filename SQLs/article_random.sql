--select * from
--(
select
   id
 , title
 , body
 , publish_date::date
 , updated_at::date updated_date
 from
   article
 where
       is_deleted = false
   and publish_date <= now()::date
 order by random()
 limit 5
--) tmp
-- order by publish_date desc

