select
   id
 , title
 , body
 , to_char(publish_date, 'Dy, DD Mon YYYY HH24:MI:SS +0900') publish_date
 , to_char(updated_at, 'Dy, DD Mon YYYY HH24:MI:SS +0900') last_modified
 from
   article
 where
       is_deleted = false
   and publish_date <= now()::date
 order by publish_date desc
 ;

