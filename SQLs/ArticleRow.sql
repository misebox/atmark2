 select
   id
 , title
 , body
 , publish_date
 , updated_at::date updated_date
 from
   article
 where
       is_deleted = false
   and publish_date <= now()::date
 order by id desc
 offset 0 limit 10;

