select
  id
, title
, body
 ,publish_date::date
, date_part('year', publish_date) publish_year
, updated_at::date updated_date
  from article
  where id = '<@val:aid/>'
    and is_deleted = false
    and publish_date <= now()
;
