atmark2
=======

is
-----
a lightweight template engine for simple web pages written in c++.

works on
- mod_fcgid
- PostgreSQL 9.3

required packages for build
-----------
### clang
clang: a C language family frontend for LLVM  
http://clang.llvm.org/  
version: 3.5  

### fcgi-devel
Development files for fcgi.  
http://www.fastcgi.com/drupal/  
Version: 2.4.0  

### libpqxx-devel
Development tools for libpqxx.  
http://pqxx.org/development/libpqxx/  
Version: 4.0.1  

### gtest-devel
Development files for gtest.  
https://code.google.com/p/googletest/  
Version: 1.7.0  
