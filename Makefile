# Makefile

# Macro
CXX = clang++ -std=c++14
LDFLAGS = -lfcgi -lpq -lpqxx
COMMON_FLAGS = $(LDFLAGS)
DEBUG_FLAGS = -g -O0 -Wall
TEST_LIBS = -lgtest -lpthread
TEST_FLAGS = -Isrcs/ -fprofile-arcs -ftest-coverage
RELEASE_FLAGS = -Ofast -Wall

CXXFLAGS     = $(DEBUG_FLAGS)
ERROR_SUMMARY = 2>&1 1>/dev/null | tee error_detail | grep エラー


########################################################################### files
PRODUCT = alog.fcgi
OWNER = apache
TEST_BIN := run_test

CONFIG  = config.ini
VIEW_DIR = views
SQL_DIR = SQLs
CSS_DIR = css
IMG_DIR = images

# deploy
DEPLOY_FILES = $(PRODUCT) $(CONFIG) $(VIEW_DIR) $(SQL_DIR) $(CSS_DIR)

# source and test directories
SRC_DIR = srcs
TEST_DIR = tests
OBJ_DIR = objs

# sources
SRC_LIST = logger.cc Utils.cc AppCommon.cc PGAdapter.cc\
       Parameters.cc FileConfig.cc\
       SQLFactory.cc Request.cc PageBuilder.cc Action.cc\
       Emitter.cc file_reader.cc
SRCS := $(addprefix $(SRC_DIR)/,$(SRC_LIST))
OBJS := $(addprefix $(OBJ_DIR)/,$(SRC_LIST:%.cc=%.o))

# tests
TEST_LIST = $(addprefix $(TEST_DIR)/,$(notdir $(wildcard $(SRC_DIR)/$(TEST_DIR)/*))) Utils.cc cache_flow.cc
TEST_SRCS := $(addprefix $(SRC_DIR)/,$(TEST_LIST:%.cc=%.o))
TEST_OBJS := $(addprefix $(OBJ_DIR)/,$(TEST_LIST:%.cc=%.o))

# dependencies
DEPS := $(addprefix $(OBJ_DIR)/,$(SRC_LIST:%.cc=%.d))

# destination
DST_DIR = ../cgi-bin
HTML_DIR = ../html


########################################################################### rule
$(PRODUCT):$(OBJS) $(SRC_DIR)/Main.cc
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o $@ $^

product:CXXFLAGS= $(RELEASE_FLAGS)
product:$(PRODUCT)

$(TEST_BIN): $(TEST_OBJS)
	$(CXX) $(CXXFLAGS) -o $(TEST_BIN) $(TEST_OBJS) $(TEST_FLAGS) $(TEST_LIBS)

$(OBJ_DIR)/tests/%.o:$(SRC_DIR)/tests/%.cc
	mkdir -p $(OBJ_DIR)/$(TEST_DIR)
	$(CXX) $(CXXFLAGS) $(TEST_FLAGS) -MMD -MP -o $@ -c $<

$(OBJ_DIR)/%.o:$(SRC_DIR)/%.cc
	mkdir -p $(OBJ_DIR)
	$(CXX) $(CXXFLAGS) -MMD -MP -o $@ -c $<

test: $(TEST_BIN)
	@./$(TEST_BIN)

coverage: $(TEST_BIN)
	@./$(TEST_BIN) > /dev/null
	@gcov -s $(SRC_DIR)/$(TEST_DIR) -o $(OBJ_DIR)/$(TEST_DIR) -r $(TEST_SRCS) | grep -A1 "^File .*\.cc"
	@grep "####" *.gcov
	@rm *.gcov

clean:
	ls $(PRODUCT) $(TEST_BIN) objs/{,tests/}*.{gcda,gcno,o,d} 2>/dev/null | xargs -I@ rm @

# remove destination
destroy:
	rm -rf $(DST_DIR)/*

# deploy all
deploy: $(DEPLOY_FILES) config view sql styles
	sudo systemctl restart httpd 
	sudo -u $(OWNER) -H mkdir -p $(DST_DIR)
	sudo -u $(OWNER) -H cp -p  $(PRODUCT) $(DST_DIR)

config: $(CONFIG)
	sudo -u $(OWNER) -H cp -p  $(CONFIG)  $(DST_DIR)

view: $(VIEW_DIR)
	sudo -u $(OWNER) -H cp -pr $(VIEW_DIR) $(DST_DIR)

sql: $(SQL_DIR)
	sudo -u $(OWNER) -H cp -pr $(SQL_DIR) $(DST_DIR)

styles: $(CSS_DIR)
	sudo -u $(OWNER) -H cp -pr $(CSS_DIR) $(HTML_DIR)
images:
	sudo -u $(OWNER) -H cp -pr $(IMG_DIR) $(HTML_DIR)

#Return code

