// Copyright 2015 misebox
#include <string>
#include "SQLFactory.h"

SQLFactory::SQLFactory(std::string prefix) : prefix(prefix) {
}

SQLFactory::~SQLFactory() {}
// キーに対応したSQLを取得する
std::string SQLFactory::get(std::string sql_id) {
  std::string sql = "";
  if (sql_map.count(sql_id) > 0) {
    sql = sql_map[sql_id];
  } else {
    std::ifstream ifs((prefix + sql_id + ".sql").c_str(), std::ios::in);
    std::string buf;
    while (getline(ifs, buf)) {
      sql = sql + buf + "\n";
    }
    sql_map.insert(std::make_pair(sql_id, sql));
  }
  return sql;
}


