// Copyright 2015 misebox
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "file_reader.h"
#include "logger.h"

file_reader::file_reader(std::string filename) : filename_(filename) {
}

/**
 * ファイルを読み込む
 */
bool file_reader::read(std::string& text) {
  std::ifstream     ifs(filename_, std::ios::in);
  if (ifs.fail()) {
      // ファイルを開けなかった
      aplog::write("failed to load :%s", filename_.c_str());
      return false;
  }
  std::size_t sz = ifs.seekg(0, std::ios::end).tellg();
  text.erase();
  if (text.capacity() < sz) {
    text.reserve(sz);
  }
  ifs.seekg(0, std::ios::beg);

  std::string buf = "";
  bool first = true;
  while (getline(ifs, buf)) {
    if (first) {
      first = false;
    } else {
      text = text + "\n";
    }
    // text = text + buf + "\n";
    text = text + buf;
  }
  aplog::write("successed to load :%s", filename_.c_str());
  return true;
}

