// Copyright 2015 misebox
#pragma once
#include <string>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <list>
#include <unordered_map>
#include "mysql_connection.h"
// #include "PageBuilder.h"

using namespace std;

class DBAdapter {
 private:
  sql::Driver *driver;
  // sql::Connection *con = NULL;
  // sql::Statement *stmt = NULL;
  // sql::PreparedStatement *prep_stmt = NULL;
  // sql::ResultSet *res = NULL;

  sql::SQLString host;
  sql::SQLString user;
  sql::SQLString pass;
  sql::SQLString port;

 public:
  DBAdapter(string host, string user, string pass);
  ~DBAdapter();
  sql::Connection *connect();
  bool execute(string sql);
  void executeAndMapping(list< map<string, string> > &res_list, string sql);
  // string bind(PageBuilder* pb, string sql);
}

