// Copyright 2015 misebox
#include <iostream>
#include <fcgi_stdio.h>
#include <string>
#include <unordered_map>
// #include <fstream>
#include <string.h>
#include "Utils.h"
#include "Parameters.h"
#include "FileConfig.h"
#include "logger.h"

FileConfig::FileConfig(std::string _filename, std::string _kvs ,std::string _fs) : Parameters(_filename, _kvs, _fs) {
  std::string all = "";

  std::ifstream ifs(_filename.c_str());
  if (ifs.fail()) {
    aplog::write("Error:Could not open %s.", _filename.c_str());
    exit(8);
  }

  uint size = Utils::getFileSize(_filename.c_str());
  char *buf = new char[size];
  memset(buf, 0, size);

  ifs.clear();
  ifs.seekg(0, std::fstream::beg);
  ifs.read(buf, size);
  ifs.close();

  all = std::string(buf);
  delete[] buf;
  buf = 0;

  // Parameters params = Parameters(all, "=", "\n");
  this->original_str = all;
  aplog::write("FileConfig %s is loaded \n", _filename.c_str());
  cutParams();
  // map<std::string, std::string>configs = params.getMap();
  // for (map<std::string, std::string>::iterator it = configs.begin(); it != configs.end(); it++) {
  //   printf("[%s]:%s\n", (*it).first.c_str(), (*it).second.c_str());
  // }
  // printf("[db.host]:%s\n", params.getParam(std::string("db.host")).c_str());
}
FileConfig::~FileConfig() {
}

// 設定値を取得
// std::string get(std::string _section, std::string _key);
// void dump();
