// Copyright 2015 misebox
#pragma once
#include <string>
#include <vector>
#include <typeinfo>
#include <unordered_map>

namespace Utils {

  std::string trim(std::string str);
  uint getFileSize(const char *filename);
  bool checkFileExist(std::string filename);
  std::string demangle(const char *mangled);
  template <class T>
  std::string getTypeID(T *obj) {
    return demangle(typeid(*obj).name());
  }
  std::string IntToString(int number);
  std::string replace(std::string base, std::string from, std::string to);
  unsigned int utf8_length(std::string str);
  std::unordered_map<std::string, std::string> extractMap(std::unordered_map<std::string, std::string>, std::string, bool cut_pfx=false);
  std::vector<std::string> split(const std::string&, char);
}
