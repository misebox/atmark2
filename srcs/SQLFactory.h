// Copyright 2015 misebox
#pragma once
#include <string>
#include "FileConfig.h"

class SQLFactory {
  std::string prefix;
  std::unordered_map<std::string, std::string> sql_map;
 public:
  SQLFactory(std::string prefix);

  ~SQLFactory();
  // キーに対応したSQLを取得する
  std::string get(std::string sql_id);
};

