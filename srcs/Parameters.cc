// Copyright 2015 misebox
#include <iostream>
#include <string>
#include <unordered_map>
#include "Parameters.h"
#include "Utils.h"

Parameters::Parameters(std::string _str, std::string _kvs, std::string _fs) {
    this->kvs = _kvs;
  this->fs  = _fs;
  this->original_str = _str;
  cutParams();
}

void Parameters::insert(std::string _str) {
    std::string key;
    std::string value;
    int eql_idx;

    eql_idx = _str.find(kvs, 0);
    if (eql_idx != (int)std::string::npos) {
        // key.assign(str, 0, eql_idx);
        // value.assign(str, eql_idx + 1, str.length() - eql_idx);
        key   = Utils::trim(_str.substr(0, eql_idx));
        value = Utils::trim(_str.substr(eql_idx + 1));
        this->params.insert(std::pair<std::string, std::string>(std::string(key), std::string(value)));
    }
}

void Parameters::cutParams() {
    std::string _str = this->original_str;
    std::string token;
    int amp_idx = 0;
    int next_amp;

    while (1) {
        next_amp = _str.find(fs, amp_idx);
        if (next_amp != (int)std::string::npos) {
            token = _str.substr(amp_idx, next_amp - amp_idx);
            this->insert(token);
        } else {
            token = _str.substr(amp_idx, _str.length() - amp_idx);
            this->insert(token);
            break;
        }
        amp_idx = next_amp + 1;
    }
}

std::string Parameters::getParam(std::string _str) {
    std::unordered_map<std::string, std::string>::iterator p;

    p = this->params.find(_str);
    if (p != this->params.end()) {
        return p->second;
    } else {
        return std::string("");
    }
}

std::unordered_map<std::string, std::string> Parameters::getMap() {
    return params;
}

