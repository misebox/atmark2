// Copyright 2015 misebox
#pragma once

#include <fcgi_stdio.h>
#include <string>
#include <unordered_map>

class Request {
  // 環境変数
  std::unordered_map<std::string, std::string> env;
  // リクエストパラメータのマップ
  std::unordered_map<std::string, std::string> params;
  // 16進数2文字を1文字にして返す
  static char hexToByte(char left, char right);
  // URLデコード
  static std::string UrlDecode(std::string encoded);
 public:
  Request();
  ~Request();
  void setMap(std::unordered_map<std::string, std::string> amap);
  std::string get(std::string key);
  std::unordered_map<std::string, std::string> getMap();
  std::unordered_map<std::string, std::string> getSub(std::string prefix);
};

