// Copyright 2015 misebox
#include "logger.h"
#include <stdarg.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <string>

using namespace aplog;

// logger
logger::~logger() {}

// text_logger
void text_logger::append(const char* str) {
    if (fp) fputs(str, fp);
    // if (fp) fprintf(fp, str);
}
text_logger::text_logger(const char * filename) {
}
text_logger::~text_logger() {
}

// buffer_logger
buffer_logger::buffer_logger(const char * filename) : logger(), filename_(filename) {}
buffer_logger::~buffer_logger() {
}
void buffer_logger::append(const char* str) {
    que.push(std::string(str));
}
void buffer_logger::flush() {
    fp = fopen(filename_, "a+");
    if (fp != NULL) {
        while (que.size() > 0) {
            auto line = que.front();
            // fprintf(fp, line.c_str());
            fputs(line.c_str(), fp);
            que.pop();
        }
        fclose(fp);
    }
}
multi_logger::multi_logger() {}
void multi_logger::append(const char* str) {
    for (auto alogger : mv_) {
        alogger->append(str);
    }
}
void multi_logger::flush() {
    for (auto alogger : mv_) {
        alogger->flush();
    }
}
void multi_logger::add(logger* plg) {mv_.push_back(plg);}
multi_logger::~multi_logger() {
    for (std::size_t i = 0; i < mv_.size(); i++) {delete mv_[i];}
}

// function
multi_logger& aplog::getMulti() {
    static multi_logger mlogger;
    return mlogger;
}

void aplog::setLogger(logger* plg) {
    getMulti().add(plg);
}

void aplog::write(const char *str, ...) {
    char buffer[1024];
    // get time
    time_t timer;
    struct tm* tm;
    timer = time(NULL);
    tm = localtime(&timer);
    const char *dt_form = "%Y-%m-%d %H:%M:%S ";
    strftime(buffer, 21, dt_form, tm);
    // PID
    pid_t pid = getpid();
    sprintf(buffer + strlen(buffer), "(%d) ", pid);
    // LogLevel
    strcat(buffer, "[INFO] ");
    va_list list;
    va_start(list, str);
    vsnprintf(buffer + strlen(buffer), sizeof(buffer) - strlen(buffer) -1 , str , list);
    va_end(list);
    strcat(buffer, "\n");
    getMulti().append(buffer);
}

void aplog::flush() {
    getMulti().flush();
}

