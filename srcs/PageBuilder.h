// Copyright 2015 misebox
#pragma once

#include <iostream>
#include <string>
#include <unordered_map>
#include "PGAdapter.h"

class PageBuilder {
  // タグ機能識別子
  static const char SIGN_TAG      = '@';
  static const char SIGN_INCLUDE  = '&';
  static const char SIGN_QUERY    = '?';
  static const char SIGN_DEFINE   = '#';
  static const char SIGN_VARIABLE = '$';
  static const char SIGN_EQUAL    = '=';

  // 識別文字列
  std::string id;
  // テンプレートからフィールド部分を抜き取った文字列
  std::string body;
  // テンプレート内に存在する挿入位置とタグ名のマップ(position, tagname)
  // ※positionが昇順となる
  std::map<int, std::string> fields;
  // 定義文字列用マップ
  std::unordered_map<std::string, std::string> define_values;
  // サブブロック用置換文字列
  std::unordered_map<std::string, std::string> block_values;
  // サブブロック
  std::unordered_map<std::string, PageBuilder *> blocks;
  // 読み込み
  static std::string loadFile(std::string filename);

 public:
  PageBuilder();
  ~PageBuilder();
  PageBuilder(std::string id);
  // テンプレート文字列をセットする
  void loadFromStream(std::istream &ist);
  // テキストファイルを読みテンプレート文字列をセットする
  void loadFromFile(std::string filename);
  // 文字列を読みテンプレート文字列をセットする
  void loadFromString(std::string body);
  // タグ名に対応するブロックを取得する
  PageBuilder *getBlock(std::string tag);
  // 解析済みテンプレートの各タグ位置に値をセットして返す
  std::string build(std::unordered_map<std::string, std::string>);
  std::string build(std::unordered_map<std::string, std::string>, bool);
  // テンプレートのブロック部分のみ値をセットして返す
  std::string build();
  // 指定ブロック位置に、値をセットした結果を挿入する
  std::string buildBlock(std::string blocktag, std::unordered_map<std::string, std::string> values);
};
class PageBuilderFactory {
  std::unordered_map<std::string, PageBuilder *> pool;
//  PageBuilderFactory();
 public:
  ~PageBuilderFactory();
  PageBuilder *get(std::string type);
};

