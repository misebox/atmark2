// Copyright 2015 misebox
#include <string>
#include <unordered_map>
#include "FileConfig.h"
#include "PGAdapter.h"
#include "PageBuilder.h"
#include "AppCommon.h"
#include "Action.h"
#include "Parameters.h"
#include "SQLFactory.h"
#include "file_reader.h"
#include "logger.h"
#include "Utils.h"

using std::string;
typedef std::unordered_map<string, string> umap_ss;

template <class T>
inline void safe_delete(T*& p) {
  delete p;
  p = 0;
}

AppCommon* AppCommon::instance = 0;

AppCommon::AppCommon() {
  // ページビルダロード
  pbf = new PageBuilderFactory();
  // 設定ファイルロード
  fc = new FileConfig("config.ini");
  configs = fc->getMap();
  std::string url = configs["site.scheme"] + "://" + configs["site.host"]
             + configs["site.path"]
             + configs["site.cginame"];
  configs.insert(std::make_pair("site.baseurl", url));

  // DB接続クラスロード
  dba = new PGAdapter(
    configs["db.host"],
    configs["db.port"],
    configs["db.user"],
    configs["db.pass"],
    configs["db.name"]
);
  // SQLファクトリロード
  sql_factory = new SQLFactory(configs["sql.prefix"]);
  // emitter factoryロード
  ef = new emitter_factory();
  // block storeロード
  block_store = new cache_flow<string>();
}

AppCommon *AppCommon::getInstance() {
  // インスタンス
  if (instance == NULL) {
    instance = new AppCommon();
  }
  return instance;
}
AppCommon::~AppCommon() {
  safe_delete(sql_factory);
  safe_delete(dba);
  safe_delete(fc);
  safe_delete(pbf);
  safe_delete(ef);
  safe_delete(instance);
}

umap_ss AppCommon::getConfig() {
  return configs;
}

// return site.* and [key].*
umap_ss AppCommon::getConfigFor(const string key) {
  auto site_cfgs = Utils::extractMap(configs, "site.");
  auto org = Utils::extractMap(configs, key + ".", true);
  site_cfgs.insert(org.begin(), org.end());
  return site_cfgs;
}

PGAdapter *AppCommon::getDBA() {
  return dba;
}

PageBuilder *AppCommon::getPB(string pageid) {
  // string filename = configs["pb.prefix"] + pageid + string(".") + configs["pb.extension"];
  return pbf->get(pageid);
}
void AppCommon::reloadPBF() {
  delete pbf;
  delete sql_factory;
  pbf = new PageBuilderFactory();
  sql_factory = new SQLFactory(configs["sql.prefix"]);
}
bool AppCommon::getExpandedEmitter(string id, emitter *&e) {
  if (expanded_views.count(id) > 0) {
          aplog::write("expanded view cache hit: %s", id.c_str());
          e = expanded_views.at(id);
        } else {
          aplog::write("expanded view cache miss: %s", id.c_str());
    return false;
        }
  return true;
}
void AppCommon::setExpandedEmitter(string id, emitter *e) {
  expanded_views[id] = e;
        aplog::write("expanded view cached: %s", id.c_str());
}
void AppCommon::reloadExpandedView() {
  expanded_views.clear();
}
bool AppCommon::getView(string id, string &content) {
  if (views.count(id) > 0) {
          aplog::write("view cache hit: %s", id.c_str());
          content = views[id];
        } else {
          aplog::write("view cache miss: %s", id.c_str());
    string filename = configs["emitter.prefix"] + id + "." + configs["emitter.extension"];
    file_reader fr(filename);
    if (fr.read(content)) {
      views.insert(make_pair(id, content));
          } else {
        return false;
      }
        }
  return true;
}
void AppCommon::reloadView() {
  views.clear();
}
bool AppCommon::getSQL(string id, string &sql) {
  if (sqls.count(id) > 0) {
          aplog::write("sql cache hit: %s", id.c_str());
          sql = sqls[id];
        } else {
          aplog::write("sql cache miss: %s", id.c_str());
    string filename = configs["sql.prefix"] + id + "." + configs["sql.extension"];
    file_reader fr(filename);
    if (fr.read(sql)) {
      sqls.insert(make_pair(id, sql));
          } else {
        return false;
      }
        }
  return true;
}
void AppCommon::reloadSQL() {
  sqls.clear();
}
bool AppCommon::getExpandedSQL(string id, emitter *&e) {
  if (expanded_sqls.count(id) > 0) {
          aplog::write("expanded sql cache hit: %s", id.c_str());
          e = expanded_sqls.at(id);
        } else {
          aplog::write("expanded sql cache miss: %s", id.c_str());
    return false;
        }
  return true;
}
void AppCommon::setExpandedSQL(string id, emitter *e) {
  expanded_sqls[id] = e;
        aplog::write("expanded sql cached: %s", id.c_str());
}
void AppCommon::reloadExpandedSQL() {
  expanded_sqls.clear();
}
emitter_factory *AppCommon::getEF() {
  return ef;
}
cache_flow<string> *AppCommon::getBlockStore() {
  return block_store;
}


