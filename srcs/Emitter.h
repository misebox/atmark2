// Copyright 2015 misebox
#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <map>
#include <utility>
#include <functional>
#include <regex>
#include <exception>

typedef std::unordered_map<std::string, std::string> umap_ss;
class context {
//friend class base_emitter;
public:
  umap_ss req_head;
  umap_ss req;
  umap_ss res_head;
  std::string res_body;
};

class base_emitter {
 public:
  base_emitter();
  virtual ~base_emitter();
  virtual void expand(const umap_ss &) = 0;
  virtual bool unite(int level) = 0;
  virtual void emit(context &) = 0;
};

class emitter : public base_emitter {
 public:
  enum captidx {
    e_tag = 1,
    e_sign,
    e_id,
    e_args,
    e_content,
  };
  std::string sign_, id_;
  umap_ss args_;
  std::vector<emitter *> inner_;
  ~emitter();
  void set_sign(const std::string);
  void set_id(const std::string);
  void set_args(const std::string);
  void append_child(emitter *);
  void load(const std::string &);
  virtual void expand(const umap_ss &);
  virtual bool unite(int level);
  virtual void emit(context &ctx);
};

class text_emitter : public emitter {
 public:
  text_emitter();
  text_emitter(std::string);
  ~text_emitter();
  std::string text;
  void expand(const umap_ss &);
  void emit(context &);
  bool unite(int level);
};
class mutable_emitter : public emitter {
 public:
  bool unite(int level);
};

// emitter
class top_emitter  : public emitter { public: void emit(context &); };
class cache_emitter : public emitter { public: void emit(context &); };
class  db_emitter  : public emitter { public: void emit(context &); };
class row_emitter  : public mutable_emitter { public: void emit(context &); };
class val_emitter  : public mutable_emitter { public: void emit(context &); };
// expanding emitter
class eol_emitter  : public text_emitter { public: void expand(const umap_ss &); };
class pre_emitter : public text_emitter { public: void expand(const umap_ss &); };
class inc_emitter  : public emitter { public: void expand(const umap_ss &); };
class wrap_emitter : public emitter { public: void expand(const umap_ss &); };
class target_emitter : public emitter {};

// factory
class emitter_factory {
 public:
  std::unordered_map<std::string, std::function<emitter *()>> emitters;
  std::unordered_map<std::string, std::function<emitter *()>> terms;
  emitter_factory();
  emitter_factory(umap_ss);
  ~emitter_factory();
  emitter *create_emitter(std::string, std::string, std::string, std::string);
};


