// Copyright 2015 misebox
#include <string>
#include <unordered_map>

#include "AppCommon.h"
#include "Request.h"
#include "FileConfig.h"
#include "PageBuilder.h"
#include "Main.h"
#include "Action.h"
#include "Emitter.h"
#include "Utils.h"

#include "logger.h"
#include <fcgi_stdio.h>
#include <chrono>
#include <thread>

using namespace Main;

void Main::OutputContent(std::string str) {
  // ヘッダ部を出力します。
  // printf("Content-type: text/html; charset=UTF-8\n\n");
  printf("%s\n\0", str.c_str());
}
// int main(int argc, char *argv[], char *envp[])
int main(int argc, char *argv[]) {
  // 共通情報クラスインスタンス生成
  app = AppCommon::getInstance();

  // ロガー設定
  auto configs = app->getConfig();
  aplog::setLogger(new aplog::buffer_logger(configs["log.path"].c_str()));
  aplog::write("===================================================== started");

  for (auto a : configs) aplog::write("configs: %s=%s", a.first.c_str(), a.second.c_str());

  // アクションファクトリ
  act_fact = new ActionFactory();

  AbstractAction *action = 0;

  while (FCGI_Accept () >= 0) {
    std::string content = "";
        auto start_time = std::chrono::system_clock::system_clock::now();
    aplog::write("----------------------------------------------------- Accepted.");
    // リクエストパラメータ取得
    Request request;
    if (argc > 1) {
      Parameters prm(argv[1], "=", "&");
      request.setMap(prm.getMap());
    }
    std::string system = request.get("system");
    // 終了コマンド
    if (system == "kill") {
      break;
    }
    if (system == "reload") {
      app->reloadPBF();
      app->reloadSQL();
      app->reloadView();
      app->reloadExpandedView();
      app->reloadExpandedSQL();
    }
    // アクション生成
    std::string actionid = request.get("action");
    if (actionid != "") {
      action = act_fact->create(actionid);
    } else {
      // アクションIDが指定されない場合はデフォルトアクション
      aplog::write("DefaultAction is %s.", configs["default.action"].c_str());
      action = act_fact->create(configs["default.action"]);
    }
    if (action != 0) {
      // アクション実行
      // TODO: 例外処理
      content = action->go(request);
      action = 0;
    }
    OutputContent(content);
        auto duration = std::chrono::system_clock::now() - start_time;
    if (system == "measure") {
      printf("<!-- [%d usec] -->\n\0", std::chrono::duration_cast<std::chrono::microseconds>(duration).count());
    }
    // FCGIレスポンス完了
    FCGI_Finish();

    // 次のリクエスト受付開始までに行う処理
    aplog::write("request processing time: %d usecs", std::chrono::duration_cast<std::chrono::microseconds>(duration).count());
    aplog::write("----------------------------------------------------- done.");
    aplog::flush();

  }
  // 終了処理
  delete act_fact;
  aplog::write("===================================================== stopped");
  // system("PAUSE");
  return EXIT_SUCCESS;
}

