// Copyright 2015 misebox
#include <stdlib.h>
#include <iostream>
#include <string>
#include <list>
#include <unordered_map>
#include <utility>

#include "PGAdapter.h"
#include "Utils.h"
#include "logger.h"

PGAdapter::PGAdapter(std::string host, std::string port, std::string user, std::string pass, std::string dbname) {
  this->host_   = host;
  this->user_   = user;
  this->pass_   = pass;
  this->port_   = port;
  this->dbname_ = dbname;
}

pqxx::connection &PGAdapter::connect() {
  static pqxx::connection *conn = 0;
  if (conn == 0) {
    std::string conn_str = std::string()
    + "dbname=" + this->dbname_
    + " user=" + this->user_
    + " password=" + this->pass_
    // + " hostaddr=" + this->host_
    // + " port=" + this->port_
    ;
    aplog::write("connect database: %s", conn_str.c_str());
    conn = new pqxx::connection(conn_str);
  }
  return *conn;
}

void PGAdapter::executeAndMapping(std::list< std::unordered_map<std::string, std::string> > &res_list, std::string sql) {
  try {
    pqxx::read_transaction work(this->connect());
    // aplog::write("SQL: %s", Utils::replace(Utils::replace(sql, "\n", " "), "  "," ").c_str());
        auto start_time = std::chrono::system_clock::system_clock::now();
    pqxx::result result = work.exec(sql);

    std::unordered_map<std::string, std::string> row;
    char dummy[10];
    for (auto res : result) {
      // 行番号
      sprintf(dummy, "%lu", res.rownumber());
      row["RowNo"] = dummy;
      // 各フィールドの値をセット
      for (auto field : res) {
        row.insert(std::make_pair(field.name(), field.as<std::string>()));
      }
      res_list.push_back(row);
      row.clear();
    }
        auto duration = std::chrono::system_clock::now() - start_time;
    aplog::write("PGAdapter: query takes %d usecs.", std::chrono::duration_cast<std::chrono::microseconds>(duration).count());
    aplog::write("PGAdapter: %d row(s), %d column(s) as result.", result.size(), result.columns());
  } catch(const std::exception &ex) {
    std::cerr << ex.what() << std::endl;
  }
}

void PGAdapter::query(std::string sql, pqxx::result &result) {
  try {
    pqxx::read_transaction work(this->connect());
    // aplog::write("SQL: %s", Utils::replace(Utils::replace(sql, "\n", " "), "  "," ").c_str());
        auto start_time = std::chrono::system_clock::system_clock::now();
    result = work.exec(sql);
        auto duration = std::chrono::system_clock::now() - start_time;
    aplog::write("PGAdapter: query takes %d usecs.", std::chrono::duration_cast<std::chrono::microseconds>(duration).count());
    aplog::write("PGAdapter: %d row(s), %d column(s) as result.", result.size(), result.columns());
  } catch(const std::exception &ex) {
    std::cerr << ex.what() << std::endl;
    aplog::write("PGAdapter: ", ex.what());
  }
}

bool PGAdapter::execute(std::string sql) {
  bool res = false;
  try {
    pqxx::work work(this->connect());
    work.exec(sql);
    work.commit();
    return true;
  } catch(const std::exception &ex) {
    std::cerr << ex.what() << std::endl;
  }
  return res;
}

PGAdapter::~PGAdapter() {
}

