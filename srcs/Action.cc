// Copyright 2015 misebox
#include <typeinfo>
#include <iostream>
#include "Action.h"
#include "Utils.h"
#include "PGAdapter.h"
#include "Utils.h"
#include "PageBuilder.h"
#include "logger.h"
#include "file_reader.h"
#include "Emitter.h"
#include "json.hpp"

using std::unordered_map;
using std::string;
using std::chrono::system_clock;
using std::chrono::duration_cast;
using std::chrono::microseconds;

using json = nlohmann::json;

// アクションファクトリ
void ActionFactory::addAction(AbstractAction *action) {
  act_map.insert(std::make_pair(action->getTypeID(), action));
}
ActionFactory::ActionFactory() {
  // ==================================================================
  // アクションを追加（新しいアクションを追加したら以下に追記すること）
  // ==================================================================
  aplog::write("ActionFactory constructed.");
  addAction(new PageAction());
  addAction(new XMLAction());
  addAction(new ErrorAction());
  addAction(new DataAction());
  addAction(new EmitterAction());
  addAction(new JSONPackAction());
}
ActionFactory::~ActionFactory() {
  for (auto sa : act_map) delete sa.second;
  aplog::write("ActionFactory destructed.");
}
AbstractAction *ActionFactory::create(string id) {
  AbstractAction *action = 0;
  // アクション処理
  string actionid = id;
  if (act_map.count(actionid + "Action") > 0) {
    auto it = act_map.find(actionid + "Action");
    action = it->second;
  } else if (act_map.count(string("ErrorAction")) > 0) {
    auto it = act_map.find("ErrorAction");
    action = it->second;
  }
  aplog::write("ActionFactory: %s is loaded.", action->getTypeID().c_str());

  return action;
}
// 抽象アクション
AbstractAction::AbstractAction()
  : app(AppCommon::getInstance())
  , dba(app->getDBA()) {
}
// 実行時型情報からクラス名文字列を取得
string AbstractAction::getTypeID() {
  return Utils::getTypeID(this);
}
// アクション実行
string AbstractAction::go(Request request) {
  // セッションID確認

  // ログインが必要
  // ログイン画面を出力
  return this->act(request);
}
// ページアクション
PageAction::PageAction() {
  aplog::write("PageAction constructed.");
}
PageAction::~PageAction() {}
string PageAction::act(Request request) {
  // クエリ補完文字列
  unordered_map<string, string> values;

  auto configs = app->getConfigFor("pb");
  string qs = Utils::replace(request.get("querystring"), "&system=reload", "");
  values.insert(std::make_pair("ThisPage", configs["site.baseurl"] + "?" + qs));
  values.insert(std::make_pair("ActionName", getTypeID()));
  values.insert(std::make_pair("Date", "2013/09/08"));
  values.insert(request.getMap().begin(), request.getMap().end());
  // - default values
  auto defaults = Utils::extractMap(configs, "default.", true);
  values.insert(defaults.begin(), defaults.end());
  values.insert(configs.begin(), configs.end());

  // BasePage
  PageBuilder *base = app->getPB("BasePage");
  string pageid(values["page"]);

  PageBuilder *inner = app->getPB(pageid);
  string content_body = inner->build(values);
  values.insert(std::make_pair("ContentBody", content_body.c_str()));
  string out = base->build(values);
        aplog::write("Output Length: %d", content_body.length());

  return out;
}
XMLAction::XMLAction() {
  aplog::write("XMLAction constructed.");
}
XMLAction::~XMLAction() {}
string XMLAction::act(Request request) {
  // クエリ補完文字列
  auto configs = app->getConfig();
  string url = "http:// "
    + configs["site.host"]
    + configs["site.path"]
    + configs["site.cginame"];
  auto values = request.getMap();
  values.insert(std::make_pair("site.baseurl", url));
        for (auto item : configs) {
          if (item.first.substr(0, 5) == "site.") {
            values.insert(item);
          }
        }
  string pageid(request.get("page"));
  PageBuilder *pb = app->getPB(pageid);
  string out = pb->build(values);
        aplog::write("Output Length: %d", out.length());
  return out;
}
// データアクション
DataAction::DataAction() {}
DataAction::~DataAction() {}
string DataAction::act(Request request) {
  // レスポンスコード
  string res_code("200");
  // リクエストマップ
  unordered_map<string, string> req_map = request.getMap();
  string data = "";
  // クエリ指定がある場合はSQLを実行
  string sql = "";
  unordered_map<string, string>::iterator it;
  if (req_map.count("query") > 0) {
    unordered_map<string, string> data_map = request.getSub("data");
    for (it = data_map.begin(); it != data_map.end(); it++) {
      string val = it->second;
      val = Utils::replace(val, "\\", "\\\\");
      val = Utils::replace(val, "'", "\\'");
      data_map[it->first] = val;
    }
    PageBuilder sqlpb;
    string queryid = req_map["query"];
    if (app->getSQL(queryid, sql)) {
      sqlpb.loadFromString(sql);
      sql = sqlpb.build(data_map);
      bool res = dba->execute(sql);
      // bool res = true;
      if (res) {
        res_code = "201";
      } else {
        res_code = "500";
      }
    }
  }

  for (it = req_map.begin(); it != req_map.end(); it++) {
    if (it != req_map.begin()) data = data + "\n,";
    data = data + "\"" + it->first + "\" : \"" + it->second + "\"";
  }
  // data = data + ",\"SQL\" : \"" + app->getSQL(req_map["query"]) + "\"";

  // レスポンスJSON生成
  // PageBuilder *pb = app->getPB("ResponseJSON");
  // unordered_map<string, string> values;
  // values.insert(std::make_pair("ResponseCode", res_code));
  // values.insert(std::make_pair("Data", " {" + data + "}"));
  // values.insert(std::make_pair("Message", Utils::replace(sql, "\n", "<br>")));
  string response = "";
  string pageid = "";
  if (req_map.count("page") > 0) {
    pageid = req_map["page"];
    PageBuilder *pb = app->getPB(pageid);
    response += "Content-type: text/html; charset=UTF-8\n\n";
    response += pb->build();
  }
  return response;
}
// Emitter
EmitterAction::EmitterAction() {
  aplog::write("EmitterAction constructed.");
}
EmitterAction::~EmitterAction() {}
string EmitterAction::act(Request request) {
auto start_time = system_clock::now();
  // config
        auto confs = app->getConfigFor("emitter");
auto duration = std::chrono::system_clock::now() - start_time;
aplog::write("elapsed time: [getConfigFor] %d usecs", duration_cast<microseconds>(duration).count());
  // request
  auto req = request.getMap();
  // - default values
start_time = system_clock::now();
  auto defaults = Utils::extractMap(confs, "default.", true);
duration = system_clock::now() - start_time;
aplog::write("elapsed time: [extractMap default.] %d usecs", duration_cast<microseconds>(duration).count());
  req.insert(defaults.begin(), defaults.end());
  // - page_url
  req.insert(std::make_pair("ThisPage", confs["site.baseurl"] + "?" + Utils::replace(req["querystring"], "&system=reload", "")));
  // logging
        for (auto a : req) aplog::write("requests: %s=%s", a.first.c_str(), a.second.c_str());

  string content;
  emitter* e;
  // load
  auto view = req["view"];
  if (!app->getExpandedEmitter(view, e)) {
    if (app->getView(view, content)) {
      e = app->getEF()->create_emitter("top", view, "", content);
      aplog::write("start building emitter.");
start_time = system_clock::now();
      e->expand(confs);
duration = system_clock::now() - start_time;
aplog::write("elapsed time: [create_emitter top] %d usecs", duration_cast<microseconds>(duration).count());
start_time = system_clock::now();
      aplog::write("measurement.");
      e->unite(1);
duration = system_clock::now() - start_time;
aplog::write("elapsed time: [unite] %d usecs", duration_cast<microseconds>(duration).count());
      // cache
start_time = system_clock::now();
      app->setExpandedEmitter(view, e);
duration = std::chrono::system_clock::now() - start_time;
aplog::write("elapsed time: [setExpandedEmitter] %d usecs", duration_cast<microseconds>(duration).count());
    } else {
      aplog::write("view is not found. please review 'config.ini'.");
      return "";
    }
  }
  aplog::write("ready to emit.");
  context ctx;
  ctx.res_body.reserve(3000);
  ctx.req = req;
start_time = system_clock::now();
  e->emit(ctx);
duration = std::chrono::system_clock::now() - start_time;
aplog::write("elapsed time: [emit] %d usecs", duration_cast<microseconds>(duration).count());
        aplog::write("Output Length: %d", ctx.res_body.length());
  return ctx.res_body;
}

// JSONPack
JSONPackAction::JSONPackAction() {
  aplog::write("JSONPackAction constructed.");
}
JSONPackAction::~JSONPackAction() {}
string JSONPackAction::act(Request request) {
  // config
  auto confs = app->getConfigFor("jsonpack");
  // request
  auto req = request.getMap();
  // - default values
  auto defaults = Utils::extractMap(confs, "default.", true);
  req.insert(defaults.begin(), defaults.end());
  // - page_url
  req.insert(std::make_pair("ThisPage", confs["site.baseurl"] + "?" + Utils::replace(req["querystring"], "&system=reload", "")));
  // logging
        for (auto a : req) aplog::write("requests: %s=%s", a.first.c_str(), a.second.c_str());

  string content;
  // load
  //auto view = req["view"];
  aplog::write("ready to emit.");
  content.reserve(3000);
auto start_time = system_clock::now();

  // JSON
  json j;
  j["templates"]["article"] =R"(
<article id="" data-attr-id="aid" class="article_view">
<header>
<h2 id="article_title" data-key="title"></h2>
<span class="publishdate">publish date: <time data-key="publish_date"></time></span>
</header>
<div style="clear:both;"></div>
<div id="article_body" data-key="article_body">
</div>
<footer>
<span class="lastupdate">
<small>
last update: <time data-key="updated_date"></time>, (c)&nbsp;<span data-key="publish_year"></span>&nbsp;misebox.
</small>
</span>
</footer>
</article>
)";

  pqxx::connection &conn = dba->connect();
  conn.prepare("articles", R"(
select
  id
, title
, body
, date_part('year', publish_date) publish_year
, updated_at::date updated_date
, now()::date now_date
  from article
  where is_deleted = false
    and publish_date <= now()::date
  order by publish_date desc
  offset $1 limit $2;
;)");
  pqxx::read_transaction work(conn);
  auto result = work.prepared("articles")(0)(5).exec();

  std::list<std::unordered_map<std::string, std::string>> res_list;
  std::unordered_map<std::string, std::string> row;
  for (auto res : result) {
    // 各フィールドの値をセット
    for (auto field : res) {
      row.insert(std::make_pair(field.name(), field.as<std::string>()));
    }

    std::string path = "/a?view=article&aid=" + row["id"];
    j["pages"][path] = {
      {"creation_time", row["now_date"] },
      {"data", {
          // {"title", row["title"] },
          {"content", {
              {"template", "article"},
              {"data", {
                  {"title", row["title"] },
                  {"publish_date", row["publish_date"]},
                  {"updated_date", row["updated_date"]},
                  {"publish_year", row["year"]},
                  {"article_body", row["body"]}
                }
              }
            }
          }
        }
      }
    };

    row.clear();
  }

  content = "Content-type: application/json; charset=UTF-8\n\n";
  content += j.dump();
auto duration = std::chrono::system_clock::now() - start_time;
aplog::write("elapsed time: [emit] %d usecs", duration_cast<microseconds>(duration).count());
  aplog::write("Output Length: %d", content.length());
  return content;
}
// エラーアクション
ErrorAction::ErrorAction() {}
ErrorAction::~ErrorAction() {}
string ErrorAction::act(Request request) {
  unordered_map<string, string> values;
  PageBuilder *pb = app->getPB("Error");
  string result = "";
  unordered_map<string, string> req_map = request.getMap();
  unordered_map<string, string>::iterator it;
  for (it = req_map.begin(); it != req_map.end(); it++) {
    result += it->first + ":" + it->second + "<br>\n";
  }
  values.insert(std::make_pair("Message", result));
  return pb->build(values);
}

