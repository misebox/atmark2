// Copyright 2015 misebox
#pragma once

#include <string>

class file_reader {
 private:
  std::string filename_;

 public:
  file_reader(std::string);
  bool read(std::string&);
};

