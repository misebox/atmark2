// Copyright 2015 misebox
#pragma once
#include <iostream>
#include <vector>
#include <mutex>
#include <queue>
#include <chrono>

namespace aplog {

class logger {
 public:
  virtual void append(const char* str)=0;
  virtual void flush() {}
  virtual ~logger();
};

class text_logger : public logger {
 public:
  void append(const char* str);
  text_logger(const char * filename);
  ~text_logger();
 private:
  text_logger(const text_logger&);
  text_logger& operator=(const text_logger&);
  FILE *fp;
};

class buffer_logger : public logger {
 public:
  void append(const char* str);
  void flush();
 public:
  buffer_logger(const char * filename);
  ~buffer_logger();
 private:
  buffer_logger(const buffer_logger&);
  buffer_logger& operator=(const buffer_logger&);
  FILE *fp;
  const char* filename_;
  std::queue<std::string> que;
  std::chrono::system_clock::time_point last_time = std::chrono::system_clock::now();
};

class multi_logger {
 public:
  multi_logger();
  void append(const char* str);
  void flush();
 public:
  void add(logger* plg);
  ~multi_logger();
 private:
  multi_logger(const multi_logger& rhs);
  multi_logger& operator=(const multi_logger& rhs);
  std::vector<logger*> mv_;
};

// global
multi_logger& getMulti();
void setLogger(logger* plg);
void write(const char *str, ...);
void flush();

}  // namespace aplog
