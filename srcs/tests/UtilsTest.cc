#include <gtest/gtest.h>
#include <stdexcept>
#include <string>
#include <stdio.h>
#include "Utils.h"

using Utils::utf8_length;
// 初期化
TEST(Utils,initialize) {
	
	ASSERT_EQ( 0, utf8_length(std::string("") ) ); // 要素数:0
	ASSERT_EQ( 6, utf8_length(std::string("123456") ) ); // 要素数:0
	ASSERT_EQ( 6, utf8_length(std::string("１２３４５６") ) ); // 要素数:0
}

TEST(Utils,utf8_length) {
	std::string u8str[] = {"UTF-8 STRING", "ＵＴＦ－８　文字列ですよ", "UTF-8　混在してます"};
	
	ASSERT_EQ( 12, utf8_length(u8str[0]) );
	ASSERT_EQ( 12, utf8_length(u8str[1]) );
	ASSERT_EQ( 12, utf8_length(u8str[2]) );
	//ASSERT_EQ( 12, std::string("あいうえおかきくけこ１２").length()); 
}
