#include <gtest/gtest.h>
#include <stdexcept>
#include <string>
#include <stdio.h>

#include <chrono>
#include <thread>

#include "cache_flow.cc"

// hitcount
TEST(cache_item, parameter) {
	std::string *body = new std::string("text");
	cache_flow<std::string>::cache_item item(body);
	std::string *got;
	got = item.get_body();
	got = item.get_body();
	got = item.get_body();

	ASSERT_EQ(*got, *body);
	ASSERT_EQ(3, item.get_hitcount());
}

// put_get
TEST(cache_flow, put_get) {
	cache_flow<std::string> cf;
	std::string key("aaa");
	std::string *body = new std::string("text");

	cf.put(key, body);

	std::string *cache;
	cache = cf.get(key);
	ASSERT_EQ(*body, *cache);
}

// put_destory_get
TEST(cache_flow, put_destroy_get) {
	cache_flow<std::string> cf;
	std::string key("aaa");
	std::string *body = new std::string("text");

	cf.put(key, body);
	cf.destroy(key);

	std::string *cache;
	cache = cf.get(key);
	ASSERT_EQ(NULL, cache);
}

// expire
TEST(cache_flow, expire) {
	cache_flow<std::string> cf;
	std::string key("aaa");
	std::string *body = new std::string("text");

	cf.put(key, body, 0.020);

	using namespace std::chrono_literals;
	std::string *cache;
	std::this_thread::sleep_for(10ms);
	cache = cf.get(key);
	ASSERT_EQ(body, cache);

	std::this_thread::sleep_for(20ms);
	cache = cf.get(key);
	ASSERT_EQ(NULL, cache);
}

//// LRU
//TEST(cache_flow, LRU) {
//	cache_flow<std::string> cf;
//	std::string key("aaa");
//	std::string body("text");
//
//	cf.put(key, &body);
//	cf.destroy(key);
//
//	std::string *cache;
//	cache = cf.get(key);
//	ASSERT_EQ(NULL, cache);
//}

