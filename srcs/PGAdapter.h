// Copyright 2015 misebox
#pragma once
#include <string>
#include <list>
#include <unordered_map>
#include <pqxx/pqxx>


class PGAdapter {
 private:
  std::string host_;
  std::string user_;
  std::string pass_;
  std::string port_;
  std::string dbname_;

 public:
  PGAdapter(std::string, std::string, std::string, std::string, std::string);
  ~PGAdapter();
  pqxx::connection &connect();
  void query(std::string sql, pqxx::result &);
  bool execute(std::string);
  void executeAndMapping(std::list< std::unordered_map<std::string, std::string> > &, std::string);
};
