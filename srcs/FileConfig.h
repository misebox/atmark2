// Copyright 2015 misebox
#pragma once
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include "Parameters.h"

class FileConfig : public Parameters {
 public:
  FileConfig(std::string _filename, std::string _kvs = "=", std::string _fs = "\n");
  ~FileConfig();
};

