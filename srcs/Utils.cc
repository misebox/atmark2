// Copyright 2015 misebox
#include <cxxabi.h>
#include <unistd.h>
#include <fcntl.h>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <sstream>
#include <typeinfo>
#include "Utils.h"
#include <stdlib.h>
#include <unordered_map>

namespace Utils {
  std::string trim(std::string str) {
    int nStart, nEnd;
    // start
    nStart = str.find_first_not_of(" \t\r\n", 0);
    if (nStart != int(std::string::npos)) {
      nEnd = str.find_last_not_of(" \t\r\n", str.length() - 1);
      if (nEnd != int(std::string::npos)) {
        return str.substr(nStart, nEnd - nStart + 1);
      } else {
        return str.substr(nStart);
      }
    } else {
      return std::string("");
    }
  }
  uint getFileSize(const char *filename) {
    // ファイルオープン。読み込み形式は指定なしのときはテキストモードになる。
    std::ifstream fs(filename);
    // ファイル末尾を探す
    fs.seekg(0, std::fstream::end);
    // ファイル末尾インデクスを取得
    uint eofPos = fs.tellg();
    // 先頭にもどるために一度clear()をかける。これをしないと次のseekg()でコケるときがある。
    fs.clear();
    // ファイル先頭に戻る
    fs.seekg(0, std::fstream::beg);
    // ファイル先頭インデクスを取得
    uint begPos = fs.tellg();
    // 末尾-先頭でファイルサイズを計算
    uint size = eofPos - begPos;

    return size;
  }
  // ファイル存在確認
  bool checkFileExist(std::string file_name) {
    int fd;
    fd = open(file_name.c_str(), O_RDONLY);
    if (fd <= 0) {
      return false;
    }
    close(fd);
    if (fd <= 0) {
      return false;
    }
    return true;
  }
  std::string demangle(const char *mangled) {
    int status;
    char *demangled = abi::__cxa_demangle(mangled, 0, 0, &status);
    std::string ret = demangled;
    free(demangled);
    demangled = 0;
    return ret;
  }

  std::string IntToString(int number) {
    std::stringstream ss;
    ss << number;
    return ss.str();
  }

  std::string replace(std::string base, std::string from, std::string to) {
    std::string result = "";
    std::string::size_type pos = 0;
    while (pos = base.find(from.c_str(), pos), pos != std::string::npos) {
      base.replace(pos, from.length(), to.c_str());
      pos += to.length();
    }
    return base;
  }

  unsigned int utf8_length(std::string str) {
    unsigned int len = 0;
    unsigned char lead;

    int char_size = 0;
    for (std::string::iterator it = str.begin(); it != str.end(); it += char_size) {

      lead = *it;
      if (lead < 0x80) {
        char_size = 1;
      } else if (lead < 0xE0) {
        char_size = 2;
      } else if (lead < 0xF0) {
        char_size = 3;
      } else {
        char_size = 4;
      }
      len += 1;
    }
    return len;
  }
std::unordered_map<std::string, std::string> extractMap(std::unordered_map<std::string, std::string> src_map, std::string pfx, bool cut_pfx) {
  std::unordered_map<std::string, std::string> dst_map;
  for (auto item : src_map) {
    if (item.first.substr(0, pfx.length()) == pfx) {
      if (cut_pfx) {
        std::string key = item.first.substr(pfx.length());
        dst_map.insert(std::make_pair(key, item.second));
      } else {
        dst_map.insert(item);
      }
    }
  }
  return dst_map;
}

std::vector<std::string> split(const std::string& input, char delimiter) {
    std::istringstream stream(input);
    std::string field;
    std::vector<std::string> result;
    while (std::getline(stream, field, delimiter)) {
        result.push_back(field);
    }
    return result;
}

}
