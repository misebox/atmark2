// Copyright 2015 misebox
/* Parameters.h */
#pragma once
#include <iostream>
#include <string>
#include <unordered_map>

class Parameters {
  std::unordered_map<std::string, std::string> params;
  std::string kvs;
  std::string fs;

  void insert(std::string _str);
 protected:
  std::string original_str;
 public:
  Parameters(std::string _str, std::string _kvs = "=", std::string _fs = "&");
  void cutParams();
  std::string getParam(std::string _key);
  std::unordered_map<std::string, std::string> getMap();
};

