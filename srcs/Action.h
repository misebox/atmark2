// Copyright 2015 misebox
#pragma once

#include "AppCommon.h"
#include <string>
#include "Request.h"

// 抽象アクション
class AbstractAction {
 protected:
  AppCommon *app;
  PGAdapter *dba;
  PageBuilder *pb;
 public:
  AbstractAction();
  virtual ~AbstractAction() {};
  virtual std::string act(Request request) = 0;
  std::string go(Request request);
  std::string getTypeID();
};

// アクションファクトリ
class ActionFactory {
  std::unordered_map<std::string, AbstractAction*> act_map;
  void addAction(AbstractAction *action);
 public:
  ActionFactory();
  ~ActionFactory();
  AbstractAction *create(std::string id);
};

// // アクション群
// class StaffAction : public AbstractAction {
// public:
//   virtual ~StaffAction();
//   virtual std::string act();
// };
// class RoleAction : public AbstractAction {
// public:
//   virtual ~RoleAction();
//   virtual std::string act();
// };
class PageAction : public AbstractAction {
 public:
  PageAction();
  virtual ~PageAction();
  virtual std::string act(Request request);
};
class XMLAction : public AbstractAction {
 public:
  XMLAction();
  virtual ~XMLAction();
  virtual std::string act(Request request);
};
class DataAction : public AbstractAction {
 public:
  DataAction();
  virtual ~DataAction();
  virtual std::string act(Request request);
};
class EmitterAction : public AbstractAction {
 public:
  EmitterAction();
  virtual ~EmitterAction();
  virtual std::string act(Request request);
};
class JSONPackAction : public AbstractAction {
 public:
  JSONPackAction();
  virtual ~JSONPackAction();
  virtual std::string act(Request request);
};
class ErrorAction : public AbstractAction {
 public:
  ErrorAction();
  virtual ~ErrorAction();
  virtual std::string act(Request request);
};

