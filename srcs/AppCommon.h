// Copyright 2015 misebox
#pragma once
#include <string>
#include <unordered_map>
#include "FileConfig.h"
#include "PageBuilder.h"
#include "SQLFactory.h"
#include "PGAdapter.h"
#include "Emitter.h"
#include "cache_flow.cc"

// 共通クラス
class AppCommon {
  // DB接続情報
  std::string host, scheme, user, pass, dbname;
  // 設定ファイル
  FileConfig *fc;
  PGAdapter *dba;
  PageBuilderFactory *pbf;
  SQLFactory *sql_factory;
  emitter_factory *ef;

  cache_flow<std::string> *block_store;

  // 設定パラメータを格納するマップ
  std::unordered_map<std::string, std::string> configs;
  // SQLを格納するマップ（展開済み）
  std::unordered_map<std::string, std::string> sqls;
  // 展開済みSQLを格納するマップ
  std::unordered_map<std::string, emitter *> expanded_sqls;
  // ビューを格納するマップ
  std::unordered_map<std::string, std::string> views;
  // 展開済みビューを格納するマップ
  std::unordered_map<std::string, emitter *> expanded_views;

  static AppCommon *instance;

  AppCommon();
 public:
  ~AppCommon();
  static AppCommon *getInstance();
  std::unordered_map<std::string, std::string> getConfig();
  // std::unordered_map<std::string, std::string> getConfigFor(const std::string);
  umap_ss getConfigFor(const std::string);
  // dba
  PGAdapter *getDBA();
  // page builder
  PageBuilder *getPB(std::string pageid);
  void reloadPBF();
  // view
  bool getView(std::string id, std::string &content);
  void reloadView();
  // expanded view
  bool getExpandedEmitter(std::string, emitter *&);
  void setExpandedEmitter(std::string, emitter *);
  void reloadExpandedView();
  // sql
  bool getSQL(std::string, std::string &);
  void reloadSQL();
  // expanded sql
  bool getExpandedSQL(std::string, emitter *&);
  void setExpandedSQL(std::string, emitter *);
  void reloadExpandedSQL();
  // facotry
  emitter_factory *getEF();
  // block store
  cache_flow<std::string> *getBlockStore();
};

