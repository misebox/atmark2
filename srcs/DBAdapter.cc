// Copyright 2015 misebox
#include <stdlib.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/resultset_metadata.h>
#include <cppconn/statement.h>
#include <cppconn/connection.h>

#include <iostream>
#include <string>
#include <list>
#include <unordered_map>
#include "mysql_connection.h"

#include "DBAdapter.h"
#include "Utils.h"

DBAdapter::DBAdapter(string _host, string _user, string _pass) {
  this->driver = get_driver_instance();
  this->host = _host;
  this->user = _user;
  this->pass = _pass;
  this->port = "3306";
}

sql::Connection *DBAdapter::connect() {
  string addr = "tcp:// " + host + ":" + port;
  sql::Connection *con
    = this->driver->connect(addr.c_str(), user.c_str(), pass.c_str());
  con->setSchema("showcase");
  return con;
}

void DBAdapter::executeAndMapping(
  list<map<string, string>> &res_list,
  string sql) {
  sql::Connection *con = 0;
  sql::Statement *stmt = 0;
  sql::ResultSet *res = 0;
  sql::ResultSetMetaData *meta = 0;
  try {
    con = this->connect();
    stmt = con->createStatement();
    res = stmt->executeQuery(sql);
    meta = res->getMetaData();

    map<string, string> row;
    size_t rowno = 1;
    char dummy[10];
    while (res->next()) {
      size_t col;
      // 行番号
      sprintf(dummy, "%lu", rowno++);
      row["RowNo"] = dummy;
      // 各フィールドの値をセット
      for (col=1; col <= meta->getColumnCount(); col++) {
        row.insert(make_pair(meta->getColumnLabel(col), res->getString(col)));
      }
      // result += pb->build(row);
      res_list.push_back(row);
      row.clear();
    }
  } catch(sql::SQLException) {
  }
  delete res;
  delete stmt;
  delete con;
}

bool DBAdapter::execute(string sql) {
  sql::Connection *con = 0;
  sql::Statement *stmt = 0;
  bool res = false;
  try {
    con = this->connect();
    stmt = con->createStatement();
    res = stmt->execute(sql);
  } catch(sql::SQLException) {
  }
  delete stmt;
  delete con;
  return res;
}

DBAdapter::~DBAdapter() {
  // delete con;
  // con = 0;
}
