// Copyright 2015 misebox
#pragma once
#include <string>
#include <iostream>
#include <unordered_map>
#include <chrono>
#include <mutex>

template <typename T>
class cache_flow {

 public:
  class cache_item {
    T *body_;
    double birth_;
    double period_;
    int hitcount_ = 0;
    double lasthit_ = 0;
 public:
    cache_item(T *body, double period = 0)
    : body_ {body}, period_ {period}
    {
      this->birth_ = this->get_now();
      // printf("birth:%0.10f\n" ,birth_);
    }

    // seconds since epoch
    double get_now() {
      double now = std::chrono::system_clock::now().time_since_epoch() / std::chrono::milliseconds(1) * 0.001;
      return now;
    }

    int get_hitcount() {
      return this->hitcount_;
    }

    double get_remain() {
      if (period_ == 0) return 0.0;
      double remain = period_ - (get_now() - birth_);
      // std::cout << "remain:" << remain << std::endl;
      return remain;
    }

    T *get_body() {
      this->hitcount_++;
      this->lasthit_ = this->get_now();
      // std::cout << "hitcount:" << hitcount_ << std::endl;
      // printf("lasthit:%0.10f\n" ,lasthit_);
      return body_;
    }

    ~cache_item() {
      delete this->body_;
    }
  };

 private:
  std::mutex mtx;
  // cache storage
  std::unordered_map<std::string, cache_item*> map_;
 public:
  cache_flow() {
  }

  ~cache_flow() {
    map_.clear();
    // std::cout << "map cleared" << std::endl;
  }

  // return cache or zero;
  T *get(std::string key) {
    std::lock_guard<std::mutex> g {mtx}; // lock
    auto it = map_.find(key);
    if (it == map_.end()) {
      // not found
      // std::cout << "not found" << std::endl;
      return NULL;
    }
    if (it->second->get_remain() < 0.0) {
      // expired
      // std::cout << "expired" << std::endl;
      return NULL;
    }
    return it->second->get_body();
  }

  void put(std::string key, T *body, double period = 0.0) {
    std::lock_guard<std::mutex> g {mtx}; // lock
    destroy(key);
    auto item = new cache_item(body, period);
    map_.insert(make_pair(key, item));
  }

  void destroy(std::string key) {
    auto it = map_.find(key);
    if (it != map_.end()) {
      // std::cout << "destroyed" << std::endl;
      map_.erase(key);
    }
  }
};

