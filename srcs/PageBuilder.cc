// Copyright 2015 misebox
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <unordered_map>
#include "PageBuilder.h"
#include "Utils.h"
#include "AppCommon.h"
#include "logger.h"

  // テンプレートからフィールド部分を抜き取った文字列
//  std::string body;
  // テンプレート内に存在するフィールド名と挿入位置リストのマップ(key, positionToInsert)
//  std::unordered_map<std::string, std::list<int>> fields;

PageBuilder *PageBuilderFactory::get(std::string pageid) {
  PageBuilder *instance;
  // インスタンスプール内に見つかれば再利用する
  if (pool.count(pageid) > 0) {
    aplog::write("template cache hit!:%s", pageid.c_str());
    instance = pool[pageid];
    // updatecheck
  } else {
    // インスタンス生成
    aplog::write("template cache miss:%s", pageid.c_str());
    instance = new PageBuilder(pageid);
    pool.insert(std::make_pair(std::string(pageid), instance));
  }
  return instance;
}
PageBuilderFactory::~PageBuilderFactory() {
  for (auto it : pool) {
    aplog::write("delete template:%s", it.first.c_str());
    delete it.second;
  }
  pool.clear();
}
PageBuilder::PageBuilder() {
}
PageBuilder::~PageBuilder() {
  std::unordered_map<std::string, PageBuilder *>::iterator it;
  for (auto it : blocks) {
    aplog::write("template cache release: %s", it.first.c_str());
    delete it.second;
  }
  blocks.clear();
}
PageBuilder::PageBuilder(std::string _id) : id(_id) {
  this->loadFromFile(id);
}
// ファイルを読み込み
std::string PageBuilder::loadFile(std::string filename) {
  std::string Prefix = AppCommon::getInstance()->getConfig()["pb.prefix"];
  std::string Extension = "ark";
  std::string fullpath = Prefix + filename + "." + Extension;
  std::string text = "";
  // ファイル存在チェック
  if (Utils::checkFileExist(fullpath)) {
    // ファイル読み込み
    std::ifstream ifs(fullpath.c_str(), std::ios::in);
    std::string buf = "";
    bool first = true;
    while (getline(ifs, buf)) {
      if (first) {
        first = false;
      } else {
        text = text + "\n";
      }
      text = text + buf;
    }
    aplog::write("load file:%s", fullpath.c_str());
  } else {
    aplog::write("file not found: %s", fullpath.c_str());
  }
  return text;
}
// テキストファイルを読みテンプレート文字列をセットする
void PageBuilder::loadFromFile(std::string filename) {
  std::string base = PageBuilder::loadFile(filename);
  // 文字列解析
  loadFromString(base);
}
// テンプレート文字列の解析処理
void PageBuilder::loadFromString(const std::string _base) {
  std::string tag = "";
  std::string base = _base + "\0";
  std::string::size_type len = base.length(), start = 0, pos, end = 0;
  std::string::size_type c_start, c_end;
  // std::string::size_type len = base.length(), start = 0, pos, end = 0;
  // size_t len = base.length(), start = 0, pos, end = 0;
  // size_t c_start, c_end;
  // <@TagName> または <@BlockName@>～</@BlockName> を探す
  while (start < len) {
    // タグ開始部分
    pos = base.find("<@", start);
    if (pos != std::string::npos) {
      // タグ終了部分
      end = base.find(">", pos + 2);
      if (end != std::string::npos) {
        // タグ名取得
        tag = base.substr(pos + 2, end - pos - 2);
        if (base.at(end - 1) == SIGN_TAG) {
          // <@～@>ならブロックタグ

          // ブロック名取得
          std::string block_name = tag.substr(0, tag.length() - 1);
          c_start = base.find("</@" + block_name + ">", end + 1);
          c_end = c_start + tag.length() + 3;
          if (c_start != std::string::npos) {
            aplog::write("PageBuilder: block %s found.", block_name.c_str());
            // ブロック内容
            std::string block_content = Utils::trim(base.substr(end + 1, c_start - end - 1));
            if (tag.at(0) == SIGN_DEFINE) {
              // ブロック定義
              define_values.insert(std::make_pair(block_name, block_content));
            } else if (tag.at(0) == SIGN_EQUAL) {
              // ブロック処理用のインスタンスを作成
              PageBuilder *block_builder = new PageBuilder();
              block_builder->loadFromString(block_content);
              blocks.insert(std::make_pair(block_name, block_builder));
            } else {
              // ブロック処理用のインスタンスを作成
              PageBuilder *block_builder = new PageBuilder();
              block_builder->loadFromString(block_content);
              blocks.insert(std::make_pair(block_name, block_builder));
            }

            // 切り取り範囲をブロックの終了まで広げる
            end = c_end;
          }
        } else {
          // シングルタグ
          if (tag.at(0) == SIGN_INCLUDE) {
            // 指定ファイルの内容をタグ部分に挿入
            std::string inc = PageBuilder::loadFile(tag.substr(1));
            base = base.substr(0, pos) + inc + base.substr(end + 1);
            len = base.length();
            aplog::write("PageBuilder: including %s.", tag.substr(1).c_str());
            continue;
          } else if (tag.at(0) == SIGN_DEFINE) {
            // 定義文字列をタグ部分に挿入
            std::string inc = "";
            if (define_values.count(tag) > 0) {
              inc = define_values[ tag ];
            }
            base = base.substr(0, pos) + inc + base.substr(end + 1);
            len = base.length();
            aplog::write("PageBuilder: use defined key [%s] => %s.", tag.c_str(), inc.c_str());
            continue;
          }
        }
        // タグ開始手前までを保存
        body += base.substr(start, pos - start);
        start = end + 1;
        // タグ名と対応する挿入位置を保存
        fields.insert(std::make_pair(body.length(), tag));
      }
    } else {
      // タグ解析終了
      body += base.substr(start);
      break;
    }
  }
}
// 解析済みテンプレートに値を挿入した文字列返す
std::string PageBuilder::build(std::unordered_map<std::string, std::string> values) {
  return build(values, false);
}
std::string PageBuilder::build(std::unordered_map<std::string, std::string> values, bool is_secure) {
  int pos = 0;
  std::string product = "";
  for (auto it : fields) {
    // タグ文字列
    std::string tag = it.second;
    // 置換文字列
    std::string replace_string = "";
    // タグの一文字目は操作識別記号
    char sign = tag.at(0);
    // タグの最後の文字が SIGN_TAG  の場合はブロックタグ
    bool is_block = tag.at(tag.length() - 1) == SIGN_TAG;

    if (sign == SIGN_VARIABLE) {
      // 変数タグ：リクエストパラメータで置換する
    } else if (sign == SIGN_QUERY) {
      // クエリタグ：クエリの結果で置換する
      std::string query_id = tag.substr(1, tag.length() - (is_block ? 2:1));
      std::string sql = "";
      if (is_block) {
        if (AppCommon::getInstance()->getSQL(query_id, sql)) {
          aplog::write("query block: %s", query_id.c_str());
          PageBuilder query_builder;
          query_builder.loadFromString(sql);
          sql = query_builder.build(values, true);

          std::list< std::unordered_map<std::string, std::string> > res_list;
          AppCommon::getInstance()->getDBA()->executeAndMapping(res_list, sql);
          for (auto row : res_list) {
            PageBuilder* block_builder = blocks["?" + query_id];
            replace_string += block_builder->build(row);
          }
        }
      }
    } else {
      if (is_secure) {
        replace_string = Utils::replace(values[tag], "'", "");
      } else {
        // 渡されたマップの値で置換する
        replace_string = values[tag];
      }
      // 置換文字列がマップに存在しない場合は、定義マップから取得
      if (replace_string == "" && values["#" + tag] != "") {
        replace_string = values["#" + tag];
        aplog::write("use defined string");
      }
      // aplog::write("replace_string: [%s] -> %s", tag.c_str(), replace_string.c_str());
    }
    // 挿入位置までの文字列とタグ置換文字列を末尾へ連結
    product = product + body.substr(pos, it.first - pos) + replace_string;
    // aplog::write("replace_string: %s", replace_string.c_str());
    // 捜査位置を進める
    pos = it.first;
  }
  // 残りを出力
  product += body.substr(pos);
  return product;
}

// テンプレートのブロック部分のみ値をセットして返す
std::string PageBuilder::build() {
  std::unordered_map<std::string, std::string> dummy;
  return build(dummy);
}

// 指定ブロック位置に、値をセットした結果を挿入する
std::string PageBuilder::buildBlock(std::string blocktag, std::unordered_map<std::string, std::string> values) {
  PageBuilder *pb = this->getBlock(blocktag);
  if (pb != 0) {
    std::unordered_map<std::string, std::string>::iterator it;
    it = block_values.find(blocktag);
    if (it == block_values.end()) {
      // 新規登録
      block_values.insert(std::make_pair(blocktag, pb->build(values)));
    } else {
      // 追加
      block_values[blocktag] = it->second + pb->build(values);
    }
  }
  return block_values[blocktag];
}
// ブロックのインスタンスを返す
PageBuilder *PageBuilder::getBlock(std::string tag) {
  std::unordered_map<std::string, PageBuilder *>::iterator it;

  it = this->blocks.find(tag);
  if (it != this->blocks.end()) {
    return it->second;
  } else {
    return 0;  // new PageBuilder();
  }
}

