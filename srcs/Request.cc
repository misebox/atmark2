// Copyright 2015 misebox
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>
#include "Parameters.h"
#include "Request.h"
#include "Utils.h"
#include "logger.h"
#include <fcgi_stdio.h>

// 16進数2文字を1文字にして返す
char Request::hexToByte(char first, char second) {
  unsigned char l, r;
  l = (first  >= 'A' ? (first  & 0xDF) - 'A' + 10 : first  - '0');
  r = (second >= 'A' ? (second & 0xDF) - 'A' + 10 : second - '0');
  if (l < 16 && r < 16) {
    return l * 16 + r;
  } else {
    return 0;
  }
}
// URLデコード
std::string Request::UrlDecode(std::string encoded) {
  size_t pos = 0;
//  std::string decoded = "";
  std::vector<char> vdec;
  while (pos < encoded.length()) {
    if (encoded.at(pos) == '%') {
      unsigned char ch = Request::hexToByte(encoded.at(pos + 1), encoded.at(pos + 2));
      if (ch > 0) {
        vdec.push_back(ch);
      }
      pos += 3;
    } else if (encoded.at(pos) == '+') {
      vdec.push_back(' ');
      pos += 1;
    } else {
      vdec.push_back(encoded.at(pos));
      pos += 1;
    }
  }
  vdec.push_back('\0');
  std::string decoded = (char*)&vdec[0];
  return decoded;
}
Request::Request() {
  // HTTPメソッド取得
  char *str;
  str = getenv("REQUEST_METHOD");

  std::string data = "";
  if (str != NULL) {
    std::string method = str;

    // http method
    if (method == "GET") {
      str = getenv("QUERY_STRING");

      if (str != NULL) {
        data = str;
      }
    } else if (method == "POST") {
      std::string len_str = getenv("CONTENT_LENGTH");
      std::istringstream istream(len_str);
      size_t length = 0;
      if (!len_str.empty()) {
        istream >> length;
      }
      char *buf = new char[ length + 1 ];
      fread(buf, 1, length, stdin);
      buf[length]  = '\0';
      data = std::string(buf);
      delete[] buf;
      std::cin.clear();
      std::cin.seekg(0);
    }
    aplog::write("HTTP Method: %s", method.c_str());
    aplog::write("ParamString: '%s'", data.c_str());
  }
  Parameters req(data, "=", "&");
  auto emap = req.getMap();
        params.insert(std::make_pair("querystring", data));
  for (auto it : emap) {
    params.insert(std::make_pair(it.first, Request::UrlDecode(it.second)));
  }
  emap.clear();
}

void Request::setMap(std::unordered_map<std::string, std::string> emap) {
  params.clear();
  std::unordered_map<std::string, std::string>::iterator it;
  for (it = emap.begin(); it != emap.end(); it++) {
    params.insert(std::make_pair(it->first, Request::UrlDecode(it->second)));
  }
}

Request::~Request() {
  params.clear();
}
std::string Request::get(std::string key) {
  std::string result;
  if (params.count(key) > 0) {
    result = params[key];
  } else {
    result = "";
  }
  return result;
}
std::unordered_map<std::string, std::string> Request::getMap() {
  return params;
}
// 文字列の最初が引数+"_"と一致するものを抽出
std::unordered_map<std::string, std::string> Request::getSub(std::string prefix) {
  std::unordered_map<std::string, std::string> submap;
  std::unordered_map<std::string, std::string>::iterator it;
  for (it = params.begin(); it != params.end(); it++) {
    if (it->first.find(prefix + "_", 0) == 0) {
      submap.insert(std::make_pair(it->first.substr(prefix.length() + 1), it->second));
    }
  }
  return submap;
}
