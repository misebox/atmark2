// Copyright 2015 misebox
#include <typeinfo>
#include <chrono>
#include "Emitter.h"
#include "logger.h"
#include "AppCommon.h"
#include "Utils.h"
#include "PGAdapter.h"

using std::regex;
using std::string;

// base_emitter
base_emitter::base_emitter() {
}
base_emitter::~base_emitter() {
}

// emitter
emitter::~emitter() {
  for (auto e : inner_) delete e;
}
void emitter::set_sign(const string sign) {
  sign_ = sign;
}
void emitter::set_id(const string id) {
  id_ = id;
}
void emitter::set_args(const string argstr) {
  auto remain = argstr;
  const regex rex("([^ \\t\\r\\n:]+)(?::([^ \\t\\r\\n]*)?)?");
  //                    1                    2
  std::smatch m;
  while (regex_search(remain, m, rex)) {
    string key = m[1];
    string val;
    if (m.length() > 2) {
      val = m[2];
    }
    args_.insert(make_pair(key, val));
    remain = m.suffix();
  }
}

void emitter::load(const string &content) {
  // 正規表現による検索のため、改行を置換しなければならない。
  string remain = Utils::replace(content, "\n", "<@eol/>");  // 改行タグ
  // 解析用正規表現
  regex rex("<(@([^ :/>]+)(?::([^ />]*))?)((?: +[^ />]*)*)(?:>(.*)</\\1>|/>)");  // NOLINT
  //  prefix| tag->1 2<-sign       3<-id       4<-args             5<-content     |suffix  // NOLINT
  std::smatch m;
  while (regex_search(remain, m, rex,
    std::regex_constants::match_default
    | std::regex_constants::match_not_eol)) {
    // タグより上の部分
    if (m.prefix().length() > 0) {
      append_child(new text_emitter(m.prefix()));
    }
    emitter_factory *ef = AppCommon::getInstance()->getEF();
    append_child(ef->create_emitter(m[e_sign], m[e_id], m[e_args], m[e_content]));
    remain = m.suffix();
  }
  // タグが見つからない場合、残りのテキストを追加
  if (remain.length() > 0) {
    append_child(new text_emitter(remain));
  }

  if (inner_.size() > 0) aplog::write("emitter@%s:%s loaded %d child%s.", sign_.c_str(), id_.c_str(), inner_.size(), inner_.size() > 1 ? "ren": "");
}
void emitter::append_child(emitter *child) {
  inner_.push_back(child);
}

void emitter::expand(const umap_ss &terms) {
  if (inner_.size() > 0 && inner_.at(0)->sign_ == "eol") {
    aplog::write("<@%s:%s> first eol found.", sign_.c_str(), id_.c_str());
    inner_.erase(inner_.begin());
  }
  for (auto child : inner_) {
    child->expand(terms);
  }
}
void emitter::emit(context& ctx) {
  for (auto child : inner_) {
    child->emit(ctx);
  }
}
bool emitter::unite(int level) {
  bool ret;
  if (inner_.size() > 0) {
    ret = true;
    // aplog::write("? %s - <@%s:%s>", string(level, '@').c_str(), sign_.c_str(), id_.c_str());
    auto it = inner_.begin();
    auto bs = inner_.end();
    auto compress = [&] () {
      if (bs != inner_.end() && bs != (it-1)) {
        // 展開済みテキスト要素をまとめる
        context buf;
        int cmp = 0;
        for (auto bi=bs; bi!=it; ++bi) {
          (*bi)->emit(buf);
          cmp++;
        }
        aplog::write("#compress %d :%s", cmp, Utils::replace(buf.res_body,"\n", "\\n").c_str());
        auto te = new text_emitter(buf.res_body);
        it = inner_.erase(bs, it);
        it = inner_.insert(it, te);
        it++;
        // continue;
      }
    };

    for (; it != inner_.end();) {
      if ((*it)->unite(level + 1)) {
        if (bs==inner_.end()) bs = it;
      } else {
        ret = false;
        //
        compress();
        bs = inner_.end();
      }
      it++;
    }
    compress();
    // aplog::write("%s %s - </@%s:%s>", ret ? "U" : "m", string(level, '@').c_str(), sign_.c_str(), id_.c_str());
  } else {
    ret = false;
    aplog::write("m %s - <@%s:%s/>", string(level, '@').c_str(), sign_.c_str(), id_.c_str());
  }
  return ret;
}
bool mutable_emitter::unite(int level) {
  aplog::write("m %s - <@%s:%s/>", string(level, '@').c_str(), sign_.c_str(), id_.c_str());
  return false;
}

// text_emitter
text_emitter::text_emitter() {
}
text_emitter::text_emitter(string src) : text(src) {
}
text_emitter::~text_emitter() {
}
void text_emitter::expand(const umap_ss &terms) {}
void text_emitter::emit(context& ctx) {
  ctx.res_body.append(text);
}
bool text_emitter::unite(int level) {
  // aplog::write("T %s - %s", string(level, '@').c_str(), Utils::replace(text,"\n", "\\n").c_str());
  return true;
}

// eol_emitter
void eol_emitter::expand(const umap_ss &terms) {
  text.assign("\n");
}

// top_emitter
void top_emitter::emit(context& ctx) {
  emitter::emit(ctx);
}

// cache_emitter
void cache_emitter::emit(context& ctx) {
  aplog::write("cache_emitter<@%s:%s/>", sign_.c_str(), id_.c_str());
  auto store = AppCommon::getInstance()->getBlockStore();
  string key;
  if (args_.count("key") > 0) {
    key = args_["key"];
  // request parameter to key
  if (args_.count("each") > 0 && ctx.req.count(args_["each"]) > 0) {
    key = key + "-" + ctx.req[ args_["each"] ];
  }
  }
  auto body = store->get(key);
  if (body == NULL) {
    // create
    context cache_ctx;
    cache_ctx.req = ctx.req;
    emitter::emit(cache_ctx);
    body = new std::string(cache_ctx.res_body);
    // enough arguments
    if (key != "" && args_.count("period") > 0) {
      double period;
      try {
        period = stod(args_["period"]);
        aplog::write("block cache created. [key:%s] [period:%f]", key.c_str(), period);
      } catch (const std::invalid_argument& ia) {
        aplog::write("cache_emitter<@%s:%s/> Invalid period: %s", sign_.c_str(), id_.c_str(), ia.what());
        period = 60;
      }
      // put cache
       store->put(key, body, period);
     } else {
      aplog::write("not enough arguments");
    }
  } else {
    aplog::write("block cache hit. [key:%s]", key.c_str());
  }
  ctx.res_body.append(*body);
}

// val_emitter
void val_emitter::emit(context& ctx) {
  if (ctx.req.count(id_) > 0) {
    ctx.res_body.append(ctx.req[id_]);
  } else {
    aplog::write("val_emitter<@%s:%s/> miss emitting.", sign_.c_str(), id_.c_str());
  }
}

// db_emitter
void db_emitter::emit(context& ctx) {
  auto app = AppCommon::getInstance();
  emitter *e;
  if (!app->getExpandedSQL(id_, e)) {
    string raw;
    if (app->getSQL(id_, raw)) {
      // リクエスト情報を適用しSQL組み立て
      // aplog::write("raw sql:%s", raw.c_str());
      emitter_factory *ef = AppCommon::getInstance()->getEF();
      e = ef->create_emitter("sql", id_, "", raw);
      e->expand(ctx.req);
      e->unite(1);
      app->setExpandedSQL(id_, e);
    } else {
      aplog::write("db_emitter<@%s:%s/> miss emitting.", sign_.c_str(), id_.c_str());
      return;
    }
  }
  context sql_ctx;
  sql_ctx.req = ctx.req;
  e->emit(sql_ctx);
  // db access
  auto start_time = std::chrono::system_clock::system_clock::now();
  pqxx::result result;
  app->getDBA()->query(sql_ctx.res_body, result);
  auto duration = std::chrono::system_clock::now() - start_time;
  aplog::write("%s:%s execute %d usecs", sign_.c_str(), id_.c_str(), std::chrono::duration_cast<std::chrono::microseconds>(duration).count());
  // クエリの結果行数分繰り返す
  for (auto row : result) {
    for (auto child : inner_) {
      if (typeid(*child) == typeid(row_emitter)) {
        try {
          ctx.res_body.append(row[child->id_].as<std::string>());
  } catch(const std::exception &ex) {
    std::cerr << ex.what() << std::endl;
    aplog::write("db_emitter: %s", ex.what());
  }
      } else {
        child->emit(ctx);
      }
    }
  }
  aplog::write("db_emitter<@%s:%s/> emitted.", sign_.c_str(), id_.c_str());
}

// row_emitter
void row_emitter::emit(context& ctx) {
  if (ctx.req.count(id_) > 0) {
    ctx.res_body.append(ctx.req[id_]);
  } else {
    if (args_.count("ifnull") > 0) {
      ctx.res_body.append(args_["ifnull"]);
    }
  }
}

// inc_emitter
void inc_emitter::expand(const umap_ss &terms) {
  string content;
  if (AppCommon::getInstance()->getView(id_, content)) {
    emitter_factory *ef = AppCommon::getInstance()->getEF();
    emitter* e = ef->create_emitter("emitter", id_, "", content);
    append_child(e);
    e->expand(terms);
    aplog::write("inc_emitter<@%s:%s/> expanded.", sign_.c_str(), id_.c_str());
  } else {
    aplog::write("inc_emitter<@%s:%s/> could not expand.", sign_.c_str(), id_.c_str());
  }
}

// wrap_emitter in:wrapper
void wrap_emitter::expand(const umap_ss &terms) {
  string wc;
  if (args_.count("in") > 0
  && AppCommon::getInstance()->getView(args_["in"], wc)) {
    emitter_factory *ef = AppCommon::getInstance()->getEF();
    emitter* e = ef->create_emitter("emitter", id_, "", wc);
    for (auto it = e->inner_.begin(); it != e->inner_.end(); ++it) {
      auto &child = *it;
      if (typeid(*child) == typeid(target_emitter)) {
        aplog::write("wrap_emitter<@%s:%s/> found target.", sign_.c_str(), id_.c_str());
        e->inner_.insert(it, inner_.begin(), inner_.end());
        e->inner_.erase(it);
        break;
      }
    }
    inner_.clear();
    append_child(e);
    e->expand(terms);
  } else {
    aplog::write("wrap_emitter<@%s:%s/> could not expand.", sign_.c_str(), id_.c_str());
    emitter::expand(terms);
  }
}

// pre_emitter
void pre_emitter::expand(const umap_ss &terms) {
  if (terms.count(id_) > 0) {
    text = terms.at(id_);
  } else {
    aplog::write("pre_emitter<@%s:%s/> miss expanding.", sign_.c_str(), id_.c_str());
  }
}

// emitter_factory
emitter_factory::emitter_factory() {
  emitters.emplace("top",    [] { return new    top_emitter(); });
  emitters.emplace("val",    [] { return new    val_emitter(); });
  emitters.emplace("eol",    [] { return new    eol_emitter(); });
  emitters.emplace("db" ,    [] { return new     db_emitter(); });
  emitters.emplace("row",    [] { return new    row_emitter(); });
  emitters.emplace("cache",  [] { return new  cache_emitter(); });
  emitters.emplace("pre" ,   [] { return new    pre_emitter(); });
  emitters.emplace("inc" ,   [] { return new    inc_emitter(); });
  emitters.emplace("wrap",   [] { return new   wrap_emitter(); });
  emitters.emplace("target", [] { return new target_emitter(); });
}
emitter *emitter_factory::create_emitter(string sign, string id, string args, string content) {
  emitter *e;
  if (emitters.count(sign) > 0) {
    e = (emitters[sign])();
  } else {
    e = new emitter();
  }
  e->set_sign(sign);
  e->set_id(id);
  e->set_args(args);
  e->load(content);
  return e;
}
emitter_factory::~emitter_factory() {
}

